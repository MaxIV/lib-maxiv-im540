#!/usr/bin/env python
     
from setuptools import setup
     
setup(name = "python-im540lib",
      version = "0.1.3",
      description = "Library for remote communication with IM540 extractor gauge controller.",
      packages = ['im540lib'],
      test_suite = "nose.collector"
     )