from functools import wraps


ERROR_TYPE = {
    "PRS": "Pressure Sensor",
    "ERR": "Error",
    "GDE": "Global Device Error",
    "ISE": "Ioni Supply Errors",
    "ISW": "Ioni Supply Warnings",
    "VSE": "Voltage Supply Errors",
    "VSW": "Voltage Supply Warnings"
}


PRESSURE_ERRORS_BITS = [1,2,3,4]


ERROR_DICT = {
    "PRS": {0: "Measurement data OK and updated (no Degas, Ranging, etc.)",
            1: "Measuring range underflow",
            2: "Measuring range overflow",
            3: "No sensor connected",
            4: "Sensor error (CODING or SUPPLY error) is pending",
            5: "Emission at the addressed ionivac sensor is ON",
            6: "Degas at the addressed ionivac sensor is ON",
            7: "Addressed ionivac sensor is selected"},

    "ERR": {2: "Receiving buffer overflow",
            3: "Invalid command or syntax error",
            4: "Parameter range error",
            5: "Command not feasible",
            6: "SW version incompatible (IM 540 <-> Profibus-SW)",
            7: "Error occurred during command execution"},

    "GDE": {0: "Watchdog responded since the device has been powered on.",
            1: "ROM error message during start-up",
            2: "RAM error message during start-up",
            3: "During start-up at least one of the CRC tests of the  "
               "EEPROMS on the MC-VP-IV or IQ board caused an error message.",
            4: "At least one SPI device has caused a timeout.",
            5: "At least one new sensor has been detected.",
            6: "Emission off because the pressure is too high.",
            7: "Emission shutdown via keyboard.",
            8: "Overtemp. signal of the power supply is active.",
            9: "Sensor status 1-4 has changed.",
            10: "Sensor status channel 1",
            11: "Sensor status channel 2",
            12: "Sensor status channel 3",
            13: "Sensor status channel 4",
            14: "The power supply has caused an error or warning.",
            15: "The ionivac power supply has caused an error or warning."},

    "ISE": {0: "Anode voltage",
            1: "Cathode voltage",
            2: "Reflector voltage",
            3: "Anode current",
            4: "Filament voltage",
            5: "Filament current",
            6: "Filament power",
            7: "---",
            8: " Cathode regulator absolute",
            9: "Cathode regulator deviation"},

    "ISW": {0: "Anode voltage",
            1: "Cathode voltage",
            2: "Reflector voltage",
            3: "Anode current",
            4: "Filament voltage",
            5: "Filament current",
            6: "Filament power",
            7: "---",
            8: "Cathode regulator absolute",
            9: "Cathode regulator deviation"},

    "VSE": {0: "Plus 5V analog",
            1: "Minus 15 V",
            2: "Plus 24 V",
            3: "Plus 15 V",
            4: "Plus 5 V",
            5: "---",
            6: "---",
            7: "---",
            8: "Plus 24 V channel 3",
            9: "Plus 24 V channel 4",
            10: "Plus 24 V KL",
            11: "Plus 5 V RS232",
            12: "Plus 15 V VB-Print",
            13: "Minus 15 V VB-Print",
            },

    "VSW": {0: " Plus 5V analog",
            1: " Minus 15 V",
            2: "Plus 24 V",
            3: "Plus 15 V",
            4: "Plus 5 V",
            5: "---",
            6: "---",
            7: "---",
            8: "Plus 24 V channel 3",
            9: "Plus 24 V channel 4",
            10: "Plus 24 V KL",
            11: "Plus 5 V RS232",
            12: "Plus 15 V VB-Print",
            13: "Minus 15 V VB-Print",
            }
}

EMISSION_CURRENT = {
    0 : '0 - AUTO',
    1 : '1 - 0.1mA',
    2 : '2 - 1mA',
    3 : '3 - 1.6mA',
    4 : '4 - 10mA'
}

SOURCE_MODE = {
    0 : "Channel controlling.",
    1 : "Only TTR starting control for ionivac systems",
    2 : "Control via analog inputs",
    3 : "Control via contact inputs",
    4 : "Control via analog and contact inputs"
}

SENSOR_MODE  = {
    0 : "Manual",
    1 : "Self",
    2 : "Auto",
    3 : "Hot"
}

def word_to_list(word, lst=None, shift=0):
    """
    Convert a word to a list of bit index
    """
    word %= 2 ** 16
    if lst is None:
        lst = []
    if not word:
        return lst
    if word % 2:
        lst.append(shift)
    return word_to_list(word >> 1, lst, shift + 1)


def get_alarm_message(alarm_type, alarm_value):
    """
    Get the corresponding alarm message
    """
    msg_dct = ERROR_DICT.get(alarm_type, None)
    if not msg_dct or not alarm_value:
        return None
    lst = word_to_list(alarm_value)
    msg = ERROR_TYPE[alarm_type] + ": \n"
    for alarm in lst:
        msg += "   - {0} : {1}\n".format(alarm, msg_dct.get(alarm, ""))
    return msg[:-1]


class Im540Exception(Exception):
    def __init__(self, msg=None, cmd=None, err_code=None):
        Exception.__init__(self, msg)
        self.cmd = cmd
        self.err_code = err_code

    def __str__(self):
        s = self.message
        if self.cmd:
            s += ' (command was "{0}")'.format(self.cmd)
        return s

def lock_it(func):
     @wraps(func)
     def wrapper(self , *args, **kwargs):
        with self.lock:
            return func(self, *args, **kwargs)
     return wrapper