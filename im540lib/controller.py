from telnetlib import Telnet
from im540lib.ressource import get_alarm_message, Im540Exception, lock_it
from im540lib.ressource import word_to_list, PRESSURE_ERRORS_BITS
from im540lib.ressource import EMISSION_CURRENT, SOURCE_MODE, SENSOR_MODE
from threading import RLock

EMISSION_ON = 1
EMISSION_OFF = 0

LF = "\x0d\x0a"
ENQ = "\x05"
ETX = "\x03"
ACK = "\x06"
NAK = "\x15"


class Im540Controller(object):
    timeout = 1

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.telnet = None
        self.lock = RLock()

    def connect(self):
        if not self.telnet:
            self.telnet = Telnet(self.host, self.port, self.timeout)

    @lock_it
    def disconnect(self):
        self.telnet.close()
        self.telnet = None

    def write(self, cmd):
        with self.lock:
            self.telnet.write(cmd + LF)
            ack = self.telnet.read_some()
        if ack == NAK + LF:
            err = self.read_answer()
            err_msg = "Negative acknowledge received with error :{0}\n".format(
                err)
            try:
                err_msg += get_alarm_message(cmd, int(err, 16))
            except TypeError:
                pass
            raise Im540Exception(err_msg, cmd, err)
        if not ack == ACK + LF:
            raise Im540Exception(
                "Invalid acknowledge received : {0}".format(ack), cmd)

    def read_answer(self):
        with self.lock:
            self.telnet.write(ENQ)
            answer = self.telnet.read_some()
        if len(answer.split(LF)) > 2:
            raise Im540Exception(
                "Read action return too many results : {0}".format(answer),
                "Read answer")
        return answer.replace(LF, "")

    @lock_it
    def ask(self, cmd):
        self.write(cmd)
        answer = self.read_answer()
        if not answer:
            raise Im540Exception("Can not read answer", cmd)
        return answer

    def reset_interface(self):
        self.telnet.write(ETX)

    def read_pressure(self, channel):
        values = self.ask("PRS, {0}".format(channel))
        bits, value = values.split(",")
        bits = int(bits, 16)
        # split error bits and info bits :
        bits_lst = word_to_list(bits)
        err = sum([2 ** bit for bit in bits_lst if bit in PRESSURE_ERRORS_BITS])
        info = sum(
            [2 ** bit for bit in bits_lst if not bit in PRESSURE_ERRORS_BITS])
        err_msg = get_alarm_message("PRS", err)
        info_msg = get_alarm_message("PRS", info)
        return float(value), err, err_msg, info, info_msg

    def is_on(self):
        values = self.ask("EMI")
        _, value = values.split(",")
        return int(value)

    def write_emission_control(self, channel, emission):
        values = self.ask("EMI, {0}, {1}".format(channel, emission))
        channel, value = values.split(",")
        return int(channel), int(value)

    def turn_on(self, channel):
        """ return channel value"""
        return self.write_emission_control(channel, EMISSION_ON)

    def turn_off(self, channel):
        return self.write_emission_control(channel, EMISSION_OFF)

    def read_emission_current(self, channel):
        value = self.ask("UEM, {0}".format(channel))
        return int(value), EMISSION_CURRENT[int(value)]

    def write_emission_current(self, channel, value):
        value = self.ask("UEM, {0}, {1}".format(channel, value))
        return int(value), EMISSION_CURRENT[int(value)]

    def reset_errors(self):
        self.write("REC, FF")

    def global_device_errors(self):
        err = self.ask("GDE")
        err_msg = get_alarm_message("GDE", int(err, 16))
        return int(err, 16), err_msg

    def ioni_supply_errors(self):
        err = self.ask("ISE")
        err_msg = get_alarm_message("ISE", int(err, 16))
        return int(err, 16), err_msg

    def ioni_supply_warnings(self):
        err = self.ask("ISW")
        err_msg = get_alarm_message("ISW", int(err, 16))
        return int(err, 16), err_msg

    def voltage_supply_errors(self):
        err = self.ask("VSE")
        err_msg = get_alarm_message("VSE", int(err, 16))
        return int(err, 16), err_msg

    def voltage_supply_warnings(self):
        err = self.ask("VSW")
        err_msg = get_alarm_message("VSW", int(err, 16))
        return int(err, 16), err_msg

    def get_errors(self):
        def merge_error(old_msg, new_msg):
            if new_msg:
                return "\n".join([old_msg, new_msg])
            return old_msg

        msg = ""
        msg = merge_error(msg, self.global_device_errors()[1])
        msg = merge_error(msg, self.ioni_supply_errors()[1])
        msg = merge_error(msg, self.ioni_supply_warnings()[1])
        msg = merge_error(msg, self.voltage_supply_errors()[1])
        msg = merge_error(msg, self.voltage_supply_warnings()[1])
        return msg

    def write_sensor_control_source(self, source):
        """ Set or query the basic type of sensor control  """
        value =  int(self.ask("SCS, {0}".format(source)))
        return value, SOURCE_MODE[value]

    def read_sensor_control_source(self,):
        """ Read or query the basic type of sensor control  """
        value =  int(self.ask("SCS"))
        return value, SOURCE_MODE[value]

    def write_sensor_control_mode(self, channel, mode):
        """ Set or query the sensor control mode."""
        value = int(self.ask("SCM, {0}, {1}".format(channel, mode)))
        return value, SENSOR_MODE[value]

    def read_sensor_control_mode(self, channel):
        """ Read or query the sensor control mode."""
        value =  int(self.ask("SCM, {0}".format(channel)))
        return value, SENSOR_MODE[value]

    def set_disable_emission_on_warning(self, enable):
        """ Set or query the behavior of the emission in case of an warning
        caused by the voltage monitoring."""
        return bool(int(self.ask("SEW, {0}".format(int(enable)))))

    def is_emission_disabled_on_warning(self):
        return bool(int(self.ask("SEW")))
