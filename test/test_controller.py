from pytest import yield_fixture, raises, main

from im540lib import controller, ressource
from mock import MagicMock, call

LF = controller.LF
ENQ = controller.ENQ
ACK = controller.ACK
NAK = controller.NAK


@yield_fixture
def im540_controller():
    telnetcls = controller.Telnet = MagicMock()
    telnet = telnetcls.return_value
    ctrl = controller.Im540Controller('192.168.127.254', 4001)
    ctrl.connect()
    telnetcls.assert_called_once_with('192.168.127.254', 4001,
                                      controller.Im540Controller.timeout)
    yield ctrl


def popper(arg):
    lst = list(reversed(arg))
    return lambda: lst.pop()


def test_acknowledge(im540_controller):
    cmd = "PRS"
    err_code = "10"
    excpected_call_cnt = 0
    # test negative acknowledge
    read_values = [NAK + LF, err_code]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    with raises(controller.Im540Exception) as context:
        im540_controller.write(cmd)
    excpected_call_cnt += 2
    assert im540_controller.telnet.write.call_count == excpected_call_cnt
    expected_message = "Negative acknowledge received with error"
    expected_error = ressource.ERROR_DICT[cmd][4]
    assert expected_message in context.value.message
    assert err_code == context.value.err_code
    assert expected_error in context.value.message
    assert cmd in str(context.value)
    # test wrong acknowledge
    read_values = ['\x05']
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    with raises(controller.Im540Exception) as context:
        im540_controller.write(cmd)
    excpected_call_cnt += 1
    assert im540_controller.telnet.write.call_count == excpected_call_cnt
    expected_message = "Invalid acknowledge received"
    assert expected_message in context.value.message
    # test positive acknowledge
    read_values = [ACK + LF]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    im540_controller.write(cmd)
    excpected_call_cnt += 1
    assert im540_controller.telnet.write.call_count == excpected_call_cnt


def test_read_answer(im540_controller):
    # read single value
    return_value = "return_simple_value"
    im540_controller.telnet.read_some.side_effect = popper([return_value + LF])
    assert im540_controller.read_answer() == return_value
    # read two values
    return_values = "first_value" + LF + "seconde_value" + LF
    im540_controller.telnet.read_some.side_effect = popper([return_values])
    with raises(controller.Im540Exception) as context:
        assert im540_controller.read_answer() == None
    expected_message = "Read action return too many results"
    assert expected_message in context.value.message
    assert "Read answer" in str(context.value)
    assert return_values in context.value.message


def test_ask(im540_controller):
    # ask value
    cmd = "PRS"
    excpected_read = "return_simple_value"
    read_values = [ACK + LF, excpected_read + LF]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value = im540_controller.ask(cmd)
    assert value == excpected_read
    # no return value
    read_values = [ACK + LF, "" + LF]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    with raises(controller.Im540Exception) as context:
        assert im540_controller.ask(cmd) == None
    expected_message = "Can not read answer"
    assert expected_message in context.value.message
    assert cmd in str(context.value)


def test_reset_interface(im540_controller):
    im540_controller.reset_interface()
    im540_controller.telnet.write.assert_called_once_with(controller.ETX)


def test_read_pressure(im540_controller):
    # no errors, no info
    read_values = [ACK + LF, "00,1.26e-5"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value, err, err_msg, info, info_msg = im540_controller.read_pressure(1)
    print im540_controller.telnet.write.call_args_list
    im540_controller.telnet.write.assert_has_calls([call("PRS, 1" + LF), call(ENQ)])
    assert value == 1.26e-5
    assert err == 0
    assert info == 0
    assert err_msg == None
    assert info_msg == None
    # error and info
    read_values = [ACK + LF, "09,1.26e-5"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value, err, err_msg, info, info_msg = im540_controller.read_pressure(1)
    assert value == 1.26e-5
    assert err == 8
    assert info == 1
    assert ressource.ERROR_DICT["PRS"][3] in err_msg
    assert ressource.ERROR_DICT["PRS"][0] in info_msg


def test_is_on(im540_controller):
    expected_cmd = "EMI"
    read_values = [ACK + LF, "1,1"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value = im540_controller.is_on()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert value == 1


def test_turn_on(im540_controller):
    expected_cmd = "EMI, 1, 1"
    read_values = [ACK + LF, "1,1"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    channel, value = im540_controller.turn_on(1)
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert value == 1
    assert channel == 1


def test_turn_off(im540_controller):
    expected_cmd = "EMI, 1, 0"
    read_values = [ACK + LF, "1,0"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    channel, value = im540_controller.turn_off(1)
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert value == 0
    assert channel == 1


def test_read_emission_current(im540_controller):
    expected_cmd = 'UEM, 1'
    read_values = [ACK + LF, "0"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value, value_str = im540_controller.read_emission_current(1)
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert value == 0
    assert value_str == ressource.EMISSION_CURRENT[0]


def test_write_emission_current(im540_controller):
    expected_cmd = 'UEM, 1, 1'
    read_values = [ACK + LF, "1"]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    value, value_str = im540_controller.write_emission_current(1, 1)
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert value == 1
    assert value_str == ressource.EMISSION_CURRENT[1]


def test_reset_errors(im540_controller):
    expected_cmd = "REC, FF"
    read_values = [ACK + LF]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    im540_controller.reset_errors()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF)])


def test_global_device_errors(im540_controller):
    expected_cmd = "GDE"
    err_value = 1
    err_bit = 0
    read_values = [ACK + LF, str(err_value)]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    err_code, err_message = im540_controller.global_device_errors()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert err_code == err_value
    assert ressource.ERROR_DICT[expected_cmd][err_bit] in err_message


def test_ioni_supply_errors(im540_controller):
    expected_cmd = "ISE"
    err_value = 1
    err_bit = 0
    read_values = [ACK + LF, str(err_value)]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    err_code, err_message = im540_controller.ioni_supply_errors()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert err_code == err_value
    assert ressource.ERROR_DICT[expected_cmd][err_bit]


def test_ioni_supply_warnings(im540_controller):
    expected_cmd = "ISW"
    err_value = 1
    err_bit = 0
    read_values = [ACK + LF, str(err_value)]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    err_code, err_message = im540_controller.ioni_supply_warnings()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert err_code == err_value
    assert ressource.ERROR_DICT[expected_cmd][err_bit] in err_message


def test_voltage_supply_errors(im540_controller):
    expected_cmd = "VSE"
    err_value = 1
    err_bit = 0
    read_values = [ACK + LF, str(err_value)]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    err_code, err_message = im540_controller.voltage_supply_errors()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert err_code == err_value
    assert ressource.ERROR_DICT[expected_cmd][err_bit] in err_message


def test_voltage_supply_warnings(im540_controller):
    expected_cmd = "VSW"
    err_value = 1
    err_bit = 0
    read_values = [ACK + LF, str(err_value)]
    im540_controller.telnet.read_some.side_effect = popper(read_values)
    err_code, err_message = im540_controller.voltage_supply_warnings()
    im540_controller.telnet.write.assert_has_calls([call(expected_cmd + LF), call(ENQ)])
    assert err_code == err_value
    assert ressource.ERROR_DICT[expected_cmd][err_bit] in err_message

# Main execution
if __name__ == "__main__":
    main()
